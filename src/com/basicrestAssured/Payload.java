package com.basicrestAssured;

public class Payload {
	
	
	public static String postPayload() {
		
		String payload ="{\r\n"
				+ "    \"credentials\": {\r\n"
				+ "        \"username\": \"dpa-api-user\",\r\n"
				+ "        \"password\": \"!23qweASD\"\r\n"
				+ "    },\r\n"
				+ "    \"packet\": {\r\n"
				+ "        \"requestReferenceID\": \"12345567891\",\r\n"
				+ "        \"merchantTransactionID\": \"agent_00045\",\r\n"
				+ "        \"requestOrigin\": \"MULA_USSD\",\r\n"
				+ "        \"checkoutType\": \"STK_PUSH\",\r\n"
				+ "        \"requestCode\": \"*369#\",\r\n"
				+ "        \"MSISDN\": \"254700000000\",\r\n"
				+ "        \"originatorMSISDN\": \"254700000000\",\r\n"
				+ "        \"serviceCode\": \"GOTVKE\",\r\n"
				+ "        \"payerClientCode\": \"SAFKE\",\r\n"
				+ "        \"language\": \"en\",\r\n"
				+ "        \"countryCode\": \"KE\",\r\n"
				+ "        \"accountNumber\": \"254700000000\",\r\n"
				+ "        \"invoiceNumber\": \"invoice_2334\",\r\n"
				+ "        \"currencyCode\": \"KES\",\r\n"
				+ "        \"amount\": 10,\r\n"
				+ "        \"requestType\": \"async\",\r\n"
				+ "        \"transactionCharges\": [],\r\n"
				+ "        \"chargeBeneficiaries\": [\r\n"
				+ "            {\r\n"
				+ "                \"beneficiaryClientCode\": \"CLIENT\",\r\n"
				+ "                \"chargeAmount\": 2.50\r\n"
				+ "            }\r\n"
				+ "        ],\r\n"
				+ "        \"description\": \"Testing Charge 3.0\",\r\n"
				+ "        \"paymentChannel\": \"paybill\",\r\n"
				+ "        \"paymentCode\": \"test\",\r\n"
				+ "        \"extraData\": {}\r\n"
				+ "    }\r\n"
				+ "}";
		
	     return payload;
	
	}
	
	

		
		
		public static String chargeSettingPayload() 
		{
		String Settingpayload ="{\r\n"
				+ "    \"payload\": {\r\n"
				+ "        \"credentials\": {\r\n"
				+ "            \"username\": \"dpa-api-user\",\r\n"
				+ "            \"password\": \"!23qweASD\"\r\n"
				+ "        },\r\n"
				+ "        \"packet\": [\r\n"
				+ "            {\r\n"
				+ "                \"serviceCode\": \"GOTVKETEST\",\r\n"
				+ "                \"payerClientCode\": \"SAFKE\",\r\n"
				+ "                \"countryCode\": \"KE\",\r\n"
				+ "                \"currencyCode\": \"KES\",\r\n"
				+ "                \"extraData\": {}\r\n"
				+ "            }\r\n"
				+ "        ]\r\n"
				+ "    }\r\n"
				+ "}";
		
		return Settingpayload;
	}


	
	



}
