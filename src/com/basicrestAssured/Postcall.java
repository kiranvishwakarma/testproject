package com.basicrestAssured;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.allOf;

import org.json.JSONArray;
import org.json.JSONObject;

import static org.hamcrest.CoreMatchers.*;

public class Postcall extends Payload{

	private static String ID;

	public static void main(String[] args) {

		RestAssured.baseURI = "https://charge-api-v3-testing.test.tingg.africa";
		// RestAssured.basePath ="/v3/Charge/Request/";
		String response = given().log().all().header("Content-Type", "application/json")
				.body(Payload.postPayload())
				.when().post("/v3/Charge/Request").then().log().all().statusCode(200).extract().response().asString();

		System.out.println(response);
	
		JSONObject js = new JSONObject(response);
		JSONArray jsonArray = (JSONArray) js.get("results");
		JSONObject Obj = (JSONObject) jsonArray.get(0);
		System.out.println("json obj -> " + Obj.toString());
		Long Id = (Long) Obj.get("chargeRequestCorrelationID");
	     System.out.println("Json object  chargeRequestCorrelationID ->" + Obj.get("chargeRequestCorrelationID"));

		
		
	     given().log().all()
				.when().get("/v3/Charge/Request/" + Id).then().assertThat().log().all().statusCode(200).extract()
				.response().asString();

		
		  
		  given().log().all() //
		  .get("v3/Charge/Request/Status/" +Id)
		  .then().assertThat().log().all().statusCode(200).extract().response().
		  asString();
		  
		  
		  given().log().all() //
		  .get("v3/Charge/Request/Beneficiaries/" +Id)
		  .then().assertThat().log().all().statusCode(200).extract().response().
		  asString();
		  
		  
		  given().log().all().header("Content-Type", "application/json")
					.body(Payload.chargeSettingPayload())
					.when().post("/v3/Charge/ChargeSettings").then()
					.log().all().statusCode(200).extract().response().asString();

		 

	}
		  

	}
