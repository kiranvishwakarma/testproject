package com.pojo;

public class ChargeBeneficiaries {
	
	public ChargeBeneficiaries ChargeBeneficiaries;
	
	
	private String beneficiaryClientCode;
	  private String chargeAmount;
	
	
  public String getBeneficiaryClientCode() {
		return beneficiaryClientCode;
	}
	public void setBeneficiaryClientCode(String beneficiaryClientCode) {
		this.beneficiaryClientCode = beneficiaryClientCode;
	}
	public String getChargeAmount() {
		return chargeAmount;
	}
	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

}
