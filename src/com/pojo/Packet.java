package com.pojo;

public class Packet {
	
	//public ChargeBeneficiaries ChargeBeneficiaries;
	
	private Packet packet;
	
	public Packet getPacket() {
		return packet;
	}
	public void setPacket(Packet packet) {
		this.packet = packet;
	}
	private String requestReferenceID;
	public String getRequestReferenceID() {
		return requestReferenceID;
	}
	public void setRequestReferenceID(String requestReferenceID) {
		this.requestReferenceID = requestReferenceID;
	}
	public String getMerchantTransactionID() {
		return merchantTransactionID;
	}
	public void setMerchantTransactionID(String merchantTransactionID) {
		this.merchantTransactionID = merchantTransactionID;
	}
	public String getRequestOrigin() {
		return requestOrigin;
	}
	public void setRequestOrigin(String requestOrigin) {
		this.requestOrigin = requestOrigin;
	}
	public String getCheckoutType() {
		return checkoutType;
	}
	public void setCheckoutType(String checkoutType) {
		this.checkoutType = checkoutType;
	}
	public String getRequestCode() {
		return requestCode;
	}
	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}
	public String getMSISDN() {
		return MSISDN;
	}
	public void setMSISDN(String mSISDN) {
		MSISDN = mSISDN;
	}
	public String getOriginatorMSISDN() {
		return originatorMSISDN;
	}
	public void setOriginatorMSISDN(String originatorMSISDN) {
		this.originatorMSISDN = originatorMSISDN;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getPayerClientCode() {
		return payerClientCode;
	}
	public void setPayerClientCode(String payerClientCode) {
		this.payerClientCode = payerClientCode;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getTransactionCharges() {
		return transactionCharges;
	}
	public void setTransactionCharges(String transactionCharges) {
		this.transactionCharges = transactionCharges;
	}
	public String getChargeBeneficiaries() {
		return chargeBeneficiaries;
	}
	public void setChargeBeneficiaries(String chargeBeneficiaries) {
		this.chargeBeneficiaries = chargeBeneficiaries;
	}
	public String getBeneficiaryClientCode() {
		return beneficiaryClientCode;
	}
	public void setBeneficiaryClientCode(String beneficiaryClientCode) {
		this.beneficiaryClientCode = beneficiaryClientCode;
	}
	public String getChargeAmount() {
		return chargeAmount;
	}
	public void setChargeAmount(String chargeAmount) {
		this.chargeAmount = chargeAmount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPaymentChannel() {
		return paymentChannel;
	}
	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}
	public String getPaymentCode() {
		return paymentCode;
	}
	public void setPaymentCode(String paymentCode) {
		this.paymentCode = paymentCode;
	}
	public String getExtraData() {
		return extraData;
	}
	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}
	private String merchantTransactionID;
	private String requestOrigin;
	private String checkoutType;
	private String requestCode;
	private String MSISDN;
	private String originatorMSISDN;
	private String serviceCode;
	private String payerClientCode;
	private String language;
	private String countryCode;
	private String accountNumber;
	private String invoiceNumber;
	private String currencyCode;
	private double  amount;
	private String requestType;
	private String transactionCharges;
	private String chargeBeneficiaries;   
	private String beneficiaryClientCode;
	private String chargeAmount;
	private String description;
	private String paymentChannel;
	private String paymentCode;
	private String extraData;

}
